// @flow

import React, {Component} from 'react';
import {AppRegistry} from 'react-native';

import App from './src/PredictablTestApp'

export default class PredictablTestApp extends Component {
  render() {
    return (
      <App />
    )
  }
}

AppRegistry.registerComponent('PredictablTest', () => PredictablTestApp);
