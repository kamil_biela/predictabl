// @flow

import {ChallengeAjaxService} from './modules/challenge/services'

export interface ServiceMap {
  challengeService: ChallengeAjaxService
}

export const services = {
  challengeService: new ChallengeAjaxService()
}

export default services