// @flow
import { ActionConst } from 'react-native-router-flux';

export type RoutesState = {
  scene: any
}

const initialState: RoutesState = {
  scene: {},
};

export default function reducer(state: RoutesState = initialState, action: any = {}) {
  switch (action.type) {
    // focus action is dispatched when a new screen comes into focus
    case ActionConst.FOCUS:
      return {
        ...state,
        scene: action.scene,
      };

    // ...other actions

    default:
      return state;
  }
}
