// @flow

import React, { Component } from 'react'
import { Router, Scene } from 'react-native-router-flux'
import { connect, Provider } from 'react-redux'
import { createEpicMiddleware } from 'redux-observable'
import { createStore, applyMiddleware, compose } from 'redux'

import rootReducer from './rootReducer'
import rootEpic from './rootEpic'
import HomeComponent from './modules/home/Home'

import {View} from 'react-native'

// create store...
const RouterWithRedux = connect()(Router);
const middleware = [createEpicMiddleware(rootEpic)];
const store = compose(
  applyMiddleware(...middleware)
)(createStore)(rootReducer);


export default class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <RouterWithRedux>
          <Scene key='root' hideNavBar={true}>
            <Scene key='home' component={HomeComponent} title='Home' />
          </Scene>
        </RouterWithRedux>
      </Provider>
    )
  }
}

