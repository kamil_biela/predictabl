// @flow

import React, { PropTypes } from 'react';
import {connect} from 'react-redux'
import { Text, View } from 'react-native';
import Button from 'react-native-button'

import type {RootState} from '../../rootReducer'
import * as challengeActions from '../challenge/actions'
import Win from '../challenge/Win'

const mapStateToProps = (state: RootState, ownProps: any) => {
  return {
    challenge: state.challengeWon.challenge
  }
}

const mapDispatchToProps = (dispatch, ownProps: any) => {
  return {
    showWonModal: () => dispatch(challengeActions.ShowWonChallenge(588))
  }
}

type HomeProps = {
  challenge: any,
  showWonModal: Function
}

class Home extends React.Component {
  props: HomeProps

  componentDidMount() {
    this.props.showWonModal()
  }

  render () {
    return (
      <View>
        {this.props.challenge && <Win challenge={this.props.challenge} />}
        <Button onPress={this.props.showWonModal}>Go to Win page</Button>
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
