// @flow
import {ServiceMap} from '../../../services'
import * as actions from '../actions'
import {Observable} from 'rxjs'

export const getChallengeEpic = (services: ServiceMap) => (action$: any) =>
  action$
    .ofType(actions.SHOW_WON_CHALLENGE)
    .mergeMap(action =>
      Observable.fromPromise(services.challengeService.fetch(action.payload))
        .map(challenge => actions.ShowWonChallengeSuccess(challenge))
        .catch(error => actions.ShowWonChallengeFail(error))
    )