// @flow

import {Challenge} from '../../../flow/api'
import * as actions from '../actions'

export type ChallengeWonState = {
  challenge: ?Challenge
}
const initialState: ChallengeWonState = {
  challenge: null
};

export function challengeWonReducer(state: ChallengeWonState = initialState, action: any = {}) {
  switch (action.type) {
    case actions.SHOW_WON_CHALLENGE_SUCCESS: {
      return {
        ...state,
        challenge: action.payload
      }
    }

    case actions.CLOSE_WON_CHALLENGE: {
      return {
        ...state,
        challenge: null
      }
    }

    default:
      return state;
  }
}
