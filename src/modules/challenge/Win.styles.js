// @flow

import {StyleSheet} from 'react-native';

const transform = [{rotate: '357deg'}]
const font = {
  fontFamily: "BebasNeue",
  color: 'white'
}

export default StyleSheet.create({
    rootView: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'stretch',
      padding: 10,
    },

    content: {
      flex: 20
    },

    contentBox: {
      backgroundColor: '#303038',
      flexGrow: 1,
    },

    contentBoxTeam: {
      flexDirection: 'row',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'flex-start'
    },

    footer: {
      flex: 15
    }
  }
);
