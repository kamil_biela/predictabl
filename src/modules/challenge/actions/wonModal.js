// @flow

import {ChallengeResponse} from '../../../flow/api'

export const SHOW_WON_CHALLENGE = "SHOW_WON_CHALLENGE"
export const ShowWonChallenge = (id: number) => ({
  type: SHOW_WON_CHALLENGE,
  payload: id
})

export const SHOW_WON_CHALLENGE_SUCCESS = "SHOW_WON_CHALLENGE_SUCCESS"
export const ShowWonChallengeSuccess = (data: ChallengeResponse) => ({
  type: SHOW_WON_CHALLENGE_SUCCESS,
  payload: data
})

export const SHOW_WON_CHALLENGE_FAIL = "SHOW_WON_CHALLENGE_FAIL"
export const ShowWonChallengeFail = (error: any) => ({
  type: SHOW_WON_CHALLENGE_FAIL,
  payload: error
})

export const CLOSE_WON_CHALLENGE = "CLOSE_WON_CHALLENGE"
export const CloseWonChallenge = () => ({
  type: CLOSE_WON_CHALLENGE,
  payload: null
})
