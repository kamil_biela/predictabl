// @flow

import ChallengeJson from '../../../data/api/challenge'
import {Challenge} from '../../../flow/api'

export class ChallengeAjaxService {
  fetch(id: number): Promise<Challenge> {
    return new Promise((resolve, reject) => {
      const challenge = ChallengeJson.find(x => x.id === id)
      if (challenge) {
        return resolve(challenge)
      }

      return reject(null)
    })
  }
}