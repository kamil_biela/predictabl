// @flow

import React, {PropTypes} from 'react';
import {connect} from 'react-redux'
import {Text, View, Modal, StyleSheet, Image} from 'react-native';
import * as challengeActions from '../challenge/actions'
import {Challenge} from '../../flow/api/challenge'
import Team from './components/Team'
import Footer from './components/Footer'
import Header from './components/Header'
import styles from './Win.styles'

type WinProps = {
  challenge: Challenge,
  closeWonModal: Function
}

const mapDispatchToProps = (dispatch, ownProps: any) => {
  return {
    closeWonModal: () => dispatch(challengeActions.CloseWonChallenge())
  }
}

const Win = (props: WinProps) => {
  const challenge = props.challenge

  if (!challenge) {
    return null
  }

  return (
    <Modal
      animationType={"slide"}
      transparent={false}
      onRequestClose={props.closeWonModal}
    >
      <Image source={require('./assets/backgroundeffect.png')} style={styles.rootView}>
        <Header />
        <View style={styles.content}>
          <Team fixture={challenge.fixture} />
        </View>
        <View style={styles.footer}>
          <Footer />
        </View>
      </Image>
    </Modal>
  )
}

export default connect(null, mapDispatchToProps)(Win);
