// @flow

import React from 'react';
import {Fixture} from '../../../flow/api';
import {Text, View, Modal, StyleSheet, Image} from 'react-native';
import styles from './Footer.styles'

type Props = {
}

const Footer = (props: Props) => {
  return (
    <View style={styles.footerTexts}>
      <Text style={styles.footerTextsTotal}>Total won</Text>
      <Text style={styles.footerTextsAmount}>
        +500
        <Image style={styles.footerTextsAmountImage} source={require('../assets/iconotcoin.png')}/>
      </Text>
      <Text style={styles.footerTextsExp}>
        +1200
        <Image style={styles.footerTextsExpImage} source={require('../assets/iconotexp.png')}/>
      </Text>
    </View>
  )
}

export default Footer
