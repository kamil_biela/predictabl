// @flow

import {StyleSheet} from 'react-native';

const transform = [{rotate: '357deg'}]

const font = {
  fontFamily: "BebasNeue",
  color: 'white'
}

const fontEffectGold = {
  textShadowColor: '#fc0',
  textShadowRadius: 1,
  textShadowOffset: {width: 1, height: 1}
}

const fontEffectShadow = {
  textShadowColor: '#000',
  textShadowRadius: 4,
  textShadowOffset: {width: 1, height: 1}
}

export default StyleSheet.create({
  header: {
    flex: 15,
  },

  headerTexts: {
    transform: transform,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },

  headerText: {
    ...font,
    ...fontEffectShadow,
    fontSize: 60,
    textAlign: 'center'
  },

  subheaderText: {
    ...font,
    ...fontEffectShadow,
    fontSize: 26,
    textAlign: 'center'
  },

  subheaderTextImage: {
    height: 20,
    width: 24,
    resizeMode: 'contain'
  }
})
