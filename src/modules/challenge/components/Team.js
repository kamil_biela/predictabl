// @flow

import React from 'react'
import {Fixture} from '../../../flow/api'
import {Text, View, Modal, StyleSheet, Image} from 'react-native'
import styles from './Team.styles'

type Props = {
  fixture: Fixture
}

const Team = (props: Props) => {
  const fixture = props.fixture

  return (
    <View style={styles.team}>
      <View style={styles.teamHeader}>
        <View style={styles.teamHome}>
          <Image style={styles.teamHomeImage} source={{uri: fixture.homeTeam.logo}}/>
          <Text style={styles.teamHomeText}>{fixture.homeTeam.name}</Text>
        </View>
        <View style={styles.teamMiddle}>
          <Image style={styles.teamMiddleImage} source={require('../assets/complogo.png')}/>
        </View>
        <View style={styles.teamAway}>
          <Text style={styles.teamAwayText}>{fixture.awayTeam.name}</Text>
          <Image style={styles.teamAwayImage} source={{uri: fixture.awayTeam.logo}}/>
        </View>
      </View>

      <View style={styles.win}>
        <View style={styles.winContainer}>
          <View style={styles.winOrLose}>
            <Image style={styles.winOrLoseImage} source={{uri: fixture.homeTeam.logo}}/>
            <Text style={styles.winOrLoseText}>Win</Text>
          </View>
          <View style={styles.collectContainer}>
            <Text style={styles.collectContainerText}>Collect 250</Text>
            <Image style={styles.collectContainerImage} source={require('../assets/iconotcoin.png')}/>
          </View>
        </View>

      </View>
    </View>
  )
}

export default Team