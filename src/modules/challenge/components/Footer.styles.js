// @flow

import {StyleSheet} from 'react-native';

const transform = [{rotate: '357deg'}]

const font = {
  fontFamily: "BebasNeue",
  color: 'white'
}

const fontEffectGold = {
  textShadowColor: '#fc0',
  textShadowRadius: 1,
  textShadowOffset: {width: 1, height: 1}
}

export default StyleSheet.create({
  footerTexts: {
    transform: transform,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },

  footerTextsTotal: {
    ...font,
    fontSize: 17,
    textAlign: 'center'
  },

  footerTextsAmount: {
    ...font,
    ...fontEffectGold,
    fontSize: 46,
    textAlign: 'center'
  },

  footerTextsExp: {
    ...font,
    ...fontEffectGold,
    fontSize: 25,
    textAlign: 'center',
    textShadowColor: "#32d900"
  },

  footerTextsExpImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },

  footerTextsAmountImage: {
    height: 30,
    width: 30,
    resizeMode: 'contain'
  }
})
