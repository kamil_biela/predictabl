// @flow

import React from 'react';
import {Text, View, Modal, StyleSheet, Image} from 'react-native';
import styles from './Header.styles'

type Props = {
}

const Header= (props: Props) => {
  return (
    <View style={styles.header}>
      <View style={styles.headerTexts}>
        <Text style={styles.headerText}>You Won</Text>
        <Text style={styles.subheaderText}>
          <Image style={styles.subheaderTextImage} source={require('./../assets/icoallchallengecard.png')}/>
          Challenges
        </Text>
      </View>
    </View>
  )
}

export default Header
