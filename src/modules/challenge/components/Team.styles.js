// @flow

import {StyleSheet} from 'react-native';

const transform = [{rotate: '357deg'}]

const font = {
  fontFamily: "BebasNeue",
  color: 'white'
}

export default StyleSheet.create({
  team: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  teamHeader: {
    flexDirection: 'row',
    height: 30,
    zIndex: 100
  },
  win: {
    zIndex: 90,
    top: -4,
    // flex: 1,
    backgroundColor: '#303038',
    flexGrow: 1,
    flexDirection: 'row',
    padding: 10,
  },
  teamHome: {backgroundColor: '#044fa1', flex: 50, marginVertical: 4},
  teamHomeImage: {position: 'absolute', left: 2, top: 2, height: 18, width: 18, paddingTop: 10},
  teamHomeText:  {position: 'absolute', left: 24, top: 3, ...font},
  teamMiddle: {backgroundColor: '#fff', width: 32, height: 32, justifyContent: 'center', alignItems: 'center'},
  teamMiddleImage: {width: 26, height: 26},
  teamAway: {backgroundColor: '#c21b2c', flex: 50, marginVertical: 4},
  teamAwayText: {position: 'absolute', right: 24, top: 3, ...font},
  teamAwayImage: {position: 'absolute', right: 2, top: 2, height: 18, width: 18, paddingTop: 10},
  winContainer: {flex: 1, flexDirection: 'column', backgroundColor: '#044fa1', justifyContent: 'center'},
  winOrLose: { flex: 7, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'},
  winOrLoseImage: {height: 48, width: 48, marginRight: 10},
  winOrLoseText: {...font, fontSize: 54},
  collectContainer: {flex: 3, flexDirection: 'row', backgroundColor: '#303038', justifyContent: 'center', alignItems: 'center', borderColor: 'yellow', borderWidth: 2},
  collectContainerText: {...font, color: 'white', fontSize: 25, marginRight: 8},
  collectContainerImage: {height: 26, width: 26}
})

