// @flow

import { combineReducers } from 'redux';
import routes from './routesReducer';

import type {ChallengeWonState} from './modules/challenge/reducers'
import type {RoutesState} from './routesReducer'

import * as challengeReducers from './modules/challenge/reducers'

export type RootState = {
  challengeWon: ChallengeWonState,
  routes: RoutesState
}

export default combineReducers({
  challengeWon: challengeReducers.challengeWonReducer,
  routes
});
