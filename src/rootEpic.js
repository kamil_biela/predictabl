// @flow

import services from './services'
import { combineEpics } from 'redux-observable'

import * as challengeEpics from './modules/challenge/epics'

const rootEpic = combineEpics(
  challengeEpics.getChallengeEpic(services)
)

export default rootEpic